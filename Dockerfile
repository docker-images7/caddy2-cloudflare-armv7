FROM caddy:builder as build-env

RUN caddy-builder \
    github.com/caddy-dns/cloudflare

FROM caddy:latest

LABEL mantainer="solomoncotton" \
    org.label-schema.name="caddy2-cloudflare-armv7" \
    org.label-schema.description="Caddy v2 Web Server on Docker for Raspberry Pi (arm7)" \
    org.label-schema.url="https://gitlab.com/solomoncotton/docker-images7/caddy2-cloudflare-armv7" \
    org.label-schema.schema-version="1.0"


COPY --from=build-env /usr/bin/caddy /usr/bin/caddy




